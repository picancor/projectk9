﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Verse;
using Verse.AI;
using UnityEngine;
using RimWorld;

namespace ProjectK9
{
    class TameablePawn : Pawn, ThoughtGiver
    {
        private static Color pawnNameColor = new Color(0.9f, 0.9f, 0.9f);
        
        public override string Label
        {
            get
            {
                if (isColonyPet())
                    return this.Name.first;
                else
                    return base.Label;
            }
        }
       
        public override string LabelShort
        {
            get
            {
                if (isColonyPet())
                    return this.Name.first;
                else
                    return base.LabelShort;
            }
        }

        public override void DrawGUIOverlay()
        {
            if (isColonyPet())
                drawPetOverlay();
            else
                base.DrawGUIOverlay();
        }
        
        private void drawPetOverlay()
        {
            if (!isColonyPet())
                return;
            Vector3 vector3 = (Vector3)GenWorldUI.LabelDrawPosFor((Thing)this, -0.6f);
            float num1 = vector3.y;

            GenFont.SetFontTiny();
            float num2 = GUI.skin.label.CalcSize(new GUIContent(this.Label)).x;
            if ((double)num2 < 20.0)
                num2 = 20f;
            Rect rect = new Rect((float)((double)vector3.x - (double)num2 / 2.0 - 4.0), vector3.y, num2 + 8f, 12f);
            GUI.DrawTexture(rect, (Texture)GenUI.GrayTextBG);
            if (this.healthTracker.Health < this.healthTracker.MaxHealth)
                Widgets.FillableBar(GenUI.GetInnerRect(rect, 1f), (float)this.healthTracker.Health / (float)this.healthTracker.MaxHealth, PawnUIOverlay.HealthTex, false, GenUI.ClearTex);
            GUI.color = pawnNameColor;
            GUI.skin.label.alignment = TextAnchor.UpperCenter;
            GUI.Label(new Rect(vector3.x - num2 / 2f, vector3.y - 2f, num2, 999f), this.Label);
            GUI.color = Color.white;
            GUI.skin.label.alignment = TextAnchor.UpperLeft;
            float num3 = num1 + 12f;
        }

        public override void DeSpawn()
        {
            if (ownership != null && ownership.ownedBed != null) { 
                ownership.ownedBed.owner = PetBed.PetBedHolder;
                ownership = null;
            }
            base.DeSpawn();
        }

        public override IEnumerable<Command> GetCommands()
        {
            if (this.Faction != Find.FactionManager.FirstFactionOfDef(FactionDef.Named("ColonyPets")))
            {
                Command_Toggle tame = new Command_Toggle();
                tame.icon = ContentFinder<Texture2D>.Get("UI/Commands/Tame");
                tame.isActive = isDesignatedToBeTamed;
                if (isDesignatedToBeTamed())
                    tame.toggleAction = removePawnTamingDesignation;
                else
                    tame.toggleAction = addPawnTamingDesignation;
                
                tame.defaultLabel = "Tame";
                tame.hotKey = KeyCode.T;
                yield return tame;
            }
            yield break;
        }

        public Thought GiveObservedThought()
        {
            if (isColonyPet() && this.RaceDef.bodySize < 0.5f)
                return new Thought_Observation(ThoughtDef.Named("SawCuteDog"), this);

            return null;
        }

        public void SetupWork()
        {
            this.apparel = new PetApparelOverride(this);

            playerController = new Pawn_PlayerController(this);
            workSettings = new Pawn_WorkSettings(this);
            carryHands = new Pawn_CarryHands(this);
            psychology = new Pawn_PsychologyTracker(this);
            story = new Pawn_StoryTracker(this);
            story.adulthood = generateStory();
            story.childhood = generateStory();

            ownership = new Pawn_Ownership(this);
            PawnBioGenerator.GiveAppropriateBioTo(this, Faction.OfColony.def);
            List<WorkTypeDef> workTypes = new List<WorkTypeDef>();

            if (this.def.defName == "Mutt")
            {
                if (!story.WorkTypeIsDisabled(getDef("Hauling")))
                    workTypes.Add(getDef("Hauling"));
            }
            else
            {
                if (!story.WorkTypeIsDisabled(getDef("Hauling")))
                    workTypes.Add(getDef("Hauling"));
                if (!story.WorkTypeIsDisabled(getDef("Hunting")))
                    workTypes.Add(getDef("Hunting"));
            }

            foreach (WorkTypeDef workType in workTypes)
                playerController.pawn.workSettings.SetPriority(workType, 4);
            
        }

        private Backstory generateStory()
        {
            Backstory backstory = new Backstory();
            backstory.bodyTypeGlobal = 0;
            backstory.workDisables = WorkTags.None;
            return backstory;
        }

        public override void ExposeData()
        {
            if (this.jobs != null && this.jobs.curDriver != null && this.jobs.curDriver.GetType().FullName.StartsWith("ProjectK9")) {
                this.jobs.EndCurrentJob(JobCondition.Errored);
            }
            base.ExposeData();
        }

        private bool isColonyPet()
        {
            return this.Faction == Find.FactionManager.FirstFactionOfDef(FactionDef.Named("ColonyPets"));
        }

        private bool isDesignatedToBeTamed()
        {
            return getTamingDesignationOnSelf() != null;
        }

        private void removePawnTamingDesignation()
        {
            Designation des = getTamingDesignationOnSelf();
            if (des != null)
                Find.DesignationManager.RemoveDesignation(des);
        }

        private Designation getTamingDesignationOnSelf()
        {
            return Find.DesignationManager.DesignationOn(this, DefDatabase<DesignationDef>.GetNamed("Tame"));
        }

        private void addPawnTamingDesignation()
        {
            Find.DesignationManager.AddDesignation(new Designation(this, DefDatabase<DesignationDef>.GetNamed("Tame")));
        }

        private WorkTypeDef getDef(string defName)
        {
            return DefDatabase<WorkTypeDef>.GetNamed(defName);
        }

        //protected override void ApplyDamage(DamageInfo dinfo)
        //{
        //    Log.Message(this + " taking " + dinfo.Amount + " damage. New health will be: " + (this.health - dinfo.Amount));
        //    base.ApplyDamage(dinfo);
        //}

        //public override void Killed(DamageInfo dam)
        //{
        //    Log.Message("Killing: " + this);
        //    base.Killed(dam);
        //    Log.Message(this + " has been killed");
        //}

        //public override void Destroy()
        //{
        //    Log.Message("Destroying: " + this);
        //    base.Destroy();
        //    Log.Message(this + " has been destroyed");
        //}
    }
}
