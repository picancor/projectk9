﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Verse;
using Verse.AI;
using RimWorld;

namespace ProjectK9.AI
{
    public class ThinkNode_SatisfyCarniverousNeeds : ThinkNode
    {
        private JobGiver_GetOrHuntFood giverGetFood = new JobGiver_GetOrHuntFood();
        private JobGiver_GetRestForAnimal giverGetRest = new JobGiver_GetRestForAnimal();
        private List<StatusLevel> statuses = new List<StatusLevel>();

        public override void PostLoad()
        {
            this.subNodes.Add(this.giverGetFood);
            this.subNodes.Add(this.giverGetRest);
        }

        public override JobPackage TryIssueJobPackage(Pawn pawn)
        {
            this.statuses.Clear();
            if (pawn.food != null && pawn.food.Food.ShouldTrySatisfy)
                this.statuses.Add(pawn.food.Food);
            if (pawn.rest != null && pawn.rest.Rest.ShouldTrySatisfy)
            {
                if (pawn.food != null && pawn.rest.Rest.CurLevel < pawn.food.Food.CurLevel)
                    this.statuses.Insert(0, pawn.rest.Rest);
                else
                    this.statuses.Add(pawn.rest.Rest);
            }
            using (List<StatusLevel>.Enumerator enumerator = this.statuses.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    StatusLevel current = enumerator.Current;
                    JobPackage jobPackage = null;
                    if (current is StatusLevel_Food)
                        jobPackage = this.giverGetFood.TryIssueJobPackage(pawn);
                    if (current is StatusLevel_Rest)
                        jobPackage = this.giverGetRest.TryIssueJobPackage(pawn);
                    if (jobPackage != null)
                        return jobPackage;
                }
            }
            return null;
        }
    }
}
