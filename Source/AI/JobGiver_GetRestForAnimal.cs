﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Verse;
using Verse.AI;
using RimWorld;

namespace ProjectK9.AI
{
    class JobGiver_GetRestForAnimal : ThinkNode_JobGiver
    {
        protected override Job TryGiveTerminalJob(Pawn pawn)
        {
            if (pawn.Faction == Find.FactionManager.FirstFactionOfDef(FactionDef.Named("ColonyPets")))
            {

                Building_Bed bedFor = pawn.ownership.ownedBed;

                if (bedFor == null)
                {
                    bedFor = findUnownedBed(pawn);

                    if (bedFor != null && bedFor.owner != pawn)
                    {
                        // If it's owned by the HolderPawn, then we need to remove that, or else the game is going to try and
                        // make it unclaim the bed and then it'll error out.
                        if (bedFor.owner == PetBed.PetBedHolder)
                        {
                            bedFor.owner = null;
                            pawn.ownership.ClaimBed(bedFor);
                        }
                        else
                            bedFor = null;
                            
                    }
                }

                if (bedFor != null)
                    return new Job(DefDatabase<JobDef>.GetNamed("SleepForAnimals"), bedFor);
            }

            return new Job(DefDatabase<JobDef>.GetNamed("SleepForAnimals"), GenCellFinder.RandomStandableClosewalkCellNear(pawn.Position, 6));
            
        }

        private Building_Bed findUnownedBed(Pawn pawn)
        {
            ThingDef petBedDef = DefDatabase<ThingDef>.GetNamed("PetBed");
            IEnumerable<PetBed> unownedBeds = Find.ListerBuildings.AllBuildingsColonistOfClass<PetBed>().Where(bed => ((PetBed)bed).owner == PetBed.PetBedHolder);

            return (PetBed)GenClosest.ClosestThingReachableGlobal(pawn.Position, unownedBeds, PathMode.OnSquare, RegionTraverseParameters.For(pawn));
        }
    }
}
