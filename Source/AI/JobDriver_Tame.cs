﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Verse;
using Verse.AI;
using RimWorld;

namespace ProjectK9.AI
{
    class JobDriver_Tame : JobDriver
    {
        public JobDriver_Tame(Pawn pawn)
            : base(pawn)
        {
        }

        protected override IEnumerable<Toil> MakeNewToils()
        {
            this.FailOnDestroyed(TargetIndex.A);
            this.FailOnThingMissingDesignation(TargetIndex.A, DefDatabase<DesignationDef>.GetNamed("Tame"));
            this.FailOnDespawned(TargetIndex.A);
            yield return Toils_Reserve.ReserveTarget(TargetIndex.A, ReservationType.Total);
            yield return Toils_Goto.GotoThing(TargetIndex.A, PathMode.Touch);

            // TODO Create "Mesmerize" Toil to stop dog from walking around and acting while being tamed.

            Toil tame = new Toil
            {
                defaultCompleteMode = ToilCompleteMode.Delay,
                defaultDuration = 20f.SecondsToTicks()
            };
            tame.AddFinishAction(tamePawn);
            yield return tame;
        }

        private void tamePawn()
        {
            TameablePawn tamee = TargetThingA as TameablePawn;

            if (tamee == null)
                return;

            Faction petFaction = getPetFaction();

            //Log.Message("Setting the pet's faction");
            tamee.SetFactionDirect(petFaction);
            //Log.Message("Setting up work on Pawn");
            tamee.SetupWork();
            //Log.Message("Giving the pawn it's Keys");
            tamee.inventory.container.TryAdd(ThingMaker.MakeThing(ThingDefOf.DoorKey));
            //Log.Message("Removing Taming designation");
            Designation tameDes = Find.DesignationManager.DesignationOn(TargetThingA, DefDatabase<DesignationDef>.GetNamed("Tame"));
            if (tameDes != null)
                Find.DesignationManager.RemoveDesignation(tameDes);

        }

        private Faction getPetFaction()
        {
            FactionDef petFactionDef = FactionDef.Named("ColonyPets");
            Faction petFaction = Find.FactionManager.FirstFactionOfDef(petFactionDef);

            if (petFaction == null)
            {
                // This should probably never happen.
                Log.Warning("Creating the Pet Faction for the First time.");
                petFaction = new Faction();
                petFaction.def = petFactionDef;
                petFaction.name = "Colony";
                Find.FactionManager.Add(petFaction);
            }

            return petFaction;
        }
    }
}
