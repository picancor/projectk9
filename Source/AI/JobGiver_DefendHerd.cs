﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Verse;
using Verse.AI;
using RimWorld;

namespace ProjectK9.AI
{
    class JobGiver_DefendHerd : ThinkNode_JobGiver
    {


        protected override Job TryGiveTerminalJob(Pawn pawn)
        {
            Pawn threat = pawn.MindState.closeThreat;
            Pawn targetOfThreat = pawn;

            if (threat == null)
            {
                IEnumerable<Pawn> herdMembers = HerdUtility.findHerdMembers(pawn);
                foreach(Pawn herdMember in herdMembers)
                {
                    if (herdMember.MindState.closeThreat != null)
                    {
                        threat = herdMember.MindState.closeThreat;
                        pawn.MindState.closeThreat = threat;
                        targetOfThreat = herdMember;
                        break;
                    }
                }
            }
                

            if (threat == null || threat.destroyed || threat.Incapacitated
                || (targetOfThreat.MindState.lastCloseThreatHarmTime - Find.TickManager.tickCount) > 300
                || (targetOfThreat.Position - threat.Position).LengthHorizontalSquared > HerdUtility.HERD_DISTANCE 
                || !GenSight.LineOfSight(pawn.Position, threat.Position))
            {
                pawn.MindState.closeThreat = null;
                return null;
            }
            else
            {
                Job job = new Job(JobDefOf.AttackMelee, threat);
                job.maxNumMeleeAttacks = 1;
                job.TimeLimit = 200;
                return job;
            }
        }
    }
}
