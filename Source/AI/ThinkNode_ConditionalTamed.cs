﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Verse;
using Verse.AI;
using RimWorld;

namespace ProjectK9.AI
{
    class ThinkNode_ConditionalTamed : ThinkNode_Priority
    {
        public bool invertResult = false;

        public override JobPackage TryIssueJobPackage(Pawn pawn)
        {
            Faction petFaction = Find.FactionManager.FirstFactionOfDef(FactionDef.Named("ColonyPets"));
            if ((!invertResult && pawn.Faction != petFaction) ||
                 (invertResult && pawn.Faction == petFaction))
                return null;

            return base.TryIssueJobPackage(pawn);

        }
    }
}
