﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Verse;
using Verse.AI;
using RimWorld;

namespace ProjectK9.AI
{
    class JobGiver_DefendColony : ThinkNode_JobGiver
    {
        protected override Job TryGiveTerminalJob(Pawn pawn)
        {
            IEnumerable<Pawn> enemiesAtHome = Find.ListerPawns.PawnsHostileToColony.Where(isThreateningHomeZone);
            Pawn closestEnemy = GenClosest.ClosestThingReachableGlobal(pawn.Position, enemiesAtHome, PathMode.Touch, RegionTraverseParameters.For(pawn)) as Pawn;

            if (closestEnemy == null || closestEnemy == pawn)
                return null;

            //Log.Warning(pawn + " found threat to colony: " + closestEnemy);
            Job job = new Job(JobDefOf.AttackMelee, closestEnemy);
            job.maxNumMeleeAttacks = 1;
            job.TimeLimit = 200;
            return job;
        }

        public bool isThreateningHomeZone(Pawn pawn)
        {
            return !pawn.IsPrisonerOfColony
                && pawn.Faction != Find.FactionManager.FirstFactionOfDef(FactionDef.Named("ColonyPets"))
                && Find.HomeRegionGrid.Get(pawn.Position) 
                && !pawn.destroyed 
                && !pawn.Incapacitated;
        }
    }
}
