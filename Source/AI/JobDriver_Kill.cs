﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Verse;
using Verse.AI;

namespace ProjectK9.AI
{
    class JobDriver_KillUnreserved : JobDriver
    {
        public JobDriver_KillUnreserved(Pawn pawn)
            : base(pawn)
        {
        }

        protected override IEnumerable<Toil> MakeNewToils()
        {
            ToilFailConditions.EndOnDespawned<JobDriver_KillUnreserved>(this, TargetIndex.A, JobCondition.Succeeded);
            this.FailOn(hunterIsKilled);
            yield return Toils_Combat.SetJobToUseToBestAttackVerb();
            Toil gotoPosition = Toils_Combat.GotoCastPosition(TargetIndex.A);
            yield return gotoPosition;
            Toil jump = Toils_Jump.JumpIfCannotHitTarget(TargetIndex.A, gotoPosition);
            yield return jump;
            Log.Message(pawn + " trying to kill " + TargetA);
            yield return Toils_Combat.CastVerb(TargetIndex.A);
            yield return Toils_Jump.Jump(jump);
            
        }

        private bool hunterIsKilled()
        {
            return pawn.destroyed || pawn.Incapacitated || pawn.healthTracker.Health == 0;
        }
    }
}
