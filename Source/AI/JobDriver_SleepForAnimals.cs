﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Verse;
using Verse.AI;
using RimWorld;

namespace ProjectK9.AI
{
    class JobDriver_SleepForAnimals : JobDriver_Sleep
    {

        private int ticksToSleepZ;
        private bool sleepingInt;

        public new bool CurrentlySleeping
        {
            get
            {
                return this.sleepingInt;
            }
        }

        public JobDriver_SleepForAnimals(Pawn pawn) : base(pawn)
        {
        }

        protected override IEnumerable<Toil> MakeNewToils()
        {
            if (pawn.jobs.curJob.targetA.HasThing)
            {
                ToilFailConditions.FailOnDespawnedOrForbidden<JobDriver_Sleep>(this, TargetIndex.A);
                yield return Toils_Reserve.ReserveTarget(TargetIndex.A, ReservationType.Total);
            }
            
            yield return Toils_Goto.GotoLoc(TargetIndex.A, PathMode.OnSquare);

            Toil sleep = new Toil();
            sleep.tickAction = new Action(tickAction);
            sleep.defaultCompleteMode = ToilCompleteMode.Never;

            yield return sleep;


        }


        private void tickAction()
        {
            this.sleepingInt = true;
            Building_Bed buildingBed = (Building_Bed)pawn.jobs.curJob.GetTarget(TargetIndex.A).Thing;
            if (buildingBed != null && buildingBed.owner != pawn)
            {
                if (pawn.Incapacitated)
                    pawn.Position = GenCellFinder.RandomStandableClosewalkCellNear(pawn.Position, 1);
                pawn.jobs.EndCurrentJob(JobCondition.Incompletable);
            }
            else
            {
                float RestEffectiveness = buildingBed == null ? 0.75f : buildingBed.def.building.bed_RestEffectiveness;
                pawn.rest.Rest.TickResting(RestEffectiveness);
                int amountToHeal = buildingBed != null ? 6 : 3;
                int tickInterval = buildingBed != null ? buildingBed.def.building.bed_HealTickInterval: 1000;

                if (Find.TickManager.tickCount % tickInterval == 0 && (pawn.healthTracker.Health < pawn.healthTracker.MaxHealth && !pawn.food.Food.Starving))
                {
                    pawn.TakeDamage(new DamageInfo(DamageTypeDefOf.Healing, amountToHeal, null));
                    if (pawn.Faction == Find.FactionManager.FirstFactionOfDef(FactionDef.Named("ColonyPets"))
                                    && pawn.healthTracker.Health == pawn.healthTracker.MaxHealth)
                    {
                        string key = "MessageFullyHealed";
                        object[] objArray = new object[1];
                        int index = 0;
                        string label = pawn.Label;
                        objArray[index] = (object)label;
                        Messages.Message(Translator.Translate(key, objArray), MessageSound.Benefit);
                    }
                }
                --this.ticksToSleepZ;
                if (this.ticksToSleepZ <= 0)
                {
                    if (!pawn.rest.DoneResting)
                        MoteMaker.ThrowSleepZ(pawn.Position);
                    if (pawn.healthTracker.Health < pawn.healthTracker.MaxHealth)
                        MoteMaker.ThrowHealingCross(pawn.Position);
                    this.ticksToSleepZ = 100;
                }
                if (pawn.Incapacitated || !pawn.rest.DoneResting || pawn.healthTracker.Health < pawn.healthTracker.MaxHealth && !pawn.food.Food.UrgentlyHungry)
                    return;
                pawn.jobs.EndCurrentJob(JobCondition.Incompletable);
            }
        }
    }
}
