﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Verse;
using Verse.AI;
using RimWorld;

namespace ProjectK9.AI
{
    class JobDriver_EatCorpse : JobDriver
    {
        public JobDriver_EatCorpse(Pawn pawn)
            : base(pawn)
        {
        }

        protected override IEnumerable<Toil> MakeNewToils()
        {
            this.FailOnDestroyed(TargetIndex.A);
            this.FailOn(eaterIsKilled); 
            yield return Toils_Reserve.ReserveTarget(TargetIndex.A, ReservationType.Total);
            yield return Toils_Goto.GotoThing(TargetIndex.A, PathMode.Touch);

            // TODO: Loop this sound
            yield return Toils_Effects.MakeSound("EatStandard");

            Toil chewCorpse = new Toil
            {
                defaultCompleteMode = ToilCompleteMode.Instant,
            };
            chewCorpse.WithEffect(EffecterDef.Named("EatMeat"), TargetIndex.A);
            chewCorpse.EndOnDespawned(TargetIndex.A);
            chewCorpse.AddFinishAction(finishEating);
            yield return chewCorpse;
        }

        private bool eaterIsKilled()
        {
            return pawn.destroyed || pawn.Incapacitated || pawn.healthTracker.Health == 0;
        }

        private void finishEating()
        {
            Corpse corpse = TargetThingA as Corpse;
            if (corpse != null) {
                
                Faction petFaction = Find.FactionManager.FirstFactionOfDef(FactionDef.Named("ColonyPets"));

                // TODO: Figure out a way to butcher a corpse without any Butcher skill
                Pawn butcher = Find.ListerPawns.FreeColonists.First();
                IEnumerable<Thing> products = corpse.ButcherProducts(butcher, 1.0f);
                List<Thing> leftovers = new List<Thing>();
                foreach (Thing meatPile in products)
                {
                    while (meatPile.stackCount > 0 && pawn.food.Food.PercentFull > 95)
                    {
                        Thing meat = meatPile.SplitOff(1);
                        pawn.food.Food.FinishEating(meat);
                    }
                    if (meatPile.stackCount > 0)
                        leftovers.Add(meatPile);
                }

                foreach (Thing leftover in leftovers)
                {
                    if (pawn.Faction != petFaction)
                        leftover.SetForbidden(true);
                    GenPlace.TryPlaceThing(leftover, corpse.Position, ThingPlaceMode.Near);
                }

                if (pawn.Faction == petFaction) 
                    pawn.psychology.thoughts.GainThought(new Thought(ThoughtDef.Named("AteStraightFromCorpse")));

                corpse.Destroy();
            }
        }

    }
}
